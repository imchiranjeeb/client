import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css'
import {Route,Switch} from 'react-router-dom';
import Navbar from './component/Navbar/Navbar';
import Home from './component/Home/Home';
import About from './component/About/About';
import Contact from './component/Contact/Contact';
import LoginForm from './component/LoginandSignup/LoginForm';
import SignupForm from './component/LoginandSignup/SignupForm';
import ErrorPage from './component/Error/ErrorPage';

const App = () => {
  return (
    <>
      <Navbar/>

      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>

        <Route exact path="/about">
          <About/>
        </Route>

        <Route exact path="/contact">
          <Contact/>
        </Route>

        <Route exact path="/login">
          <LoginForm/>
        </Route>

        <Route exact path="/signup">
          <SignupForm/>
        </Route>

        <Route>
          <ErrorPage/>
        </Route>
      </Switch>

    </>
  )
}

export default App
