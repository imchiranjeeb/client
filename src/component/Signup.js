import React from 'react';
import {Link} from 'react-router-dom';
import SignupImg from '../images/ditimg.jpeg';
import LastPageOutlinedIcon from '@material-ui/icons/LastPageOutlined';
//import './Signup.css'

const Signup = () => {
    return (
        <>
            <div className="content">
                <h2>Sign Up</h2>
                <form id="signup-form">
                    <div className="form-col">
                        <div className="col">
                            <i class="zmdi zmdi-account-circle"></i>
                            <input type="text" className="#" id="name" name="name" placeholder="Enter Your Name "/>
                        </div>
                        <br/>
                        <div className="col">
                            <i class="zmdi zmdi-email"></i>
                            <input type="email" name="email" id="email" placeholder="Enter Your Email "/>
                        </div>
                        <br/>
                        <div className="col">
                        <i class="zmdi zmdi-phone-in-talk"></i>
                            <input type="number" name="phone" id="phone" placeholder="Enter Your Phone Number "/>
                        </div>
                        <br/>
                        <div className="col">
                            <i class="zmdi zmdi-laptop-mac"></i>
                            <input type="text" name="work" id="work" placeholder="Enter Your Profession "/>
                        </div>
                        <br/>
                        <div className="col">
                            <i class="zmdi zmdi-key"></i>
                            <input type="password" name="password" id="password" placeholder="Enter Your Password "/>
                        </div>
                        <br/>
                        <div className="col">
                            <i class="zmdi zmdi-key"></i>
                            <input type="password" name="cpassword" id="cpassword" placeholder="Confirm Password "/>
                        </div>
                        <br/>
                        <div className="#">
                            <button type="submit" id="signup" name="signup">Signup <LastPageOutlinedIcon/></button>&nbsp; &nbsp; &nbsp;
                        </div>
                        <Link to="/login">Already have an account ?</Link>
                    </div>
                </form>
            </div>
        </>
    )
}

export default Signup
