import React from 'react'
import './Contact.css'

const Contact = () => {
    return (
        <>
            <div id="cntc">
                <div className="container-sm">
                    <div className="row g-9 p-2">
                        <div className="col-sm-3 p-2">
                            <div className="background">
                                PHONE<br/>
                                9348768387
                            </div>
                        </div>
                        <div className="col-sm-3 p-2">
                            <div className="background">
                                EMAIL<br/>
                                chandansahoo438@gmail.com
                            </div>
                        </div>
                        <div className="col-sm-3 p-2">
                             <div className="background">
                                ADDRESS<br/>
                                Bhuban,Odisha
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-sm p-5">
                    <div className="contact-background">
                        <h2 id="git">Get in Touch</h2>
                        <form method="post">
                            <div className="row">
                                <div className="col-md">
                                    <input type="text" name="contact_form_name" id="contact_form_name" className="contact_form_name" placeholder="Your Name :"/>&nbsp; &nbsp;
                                    <input type="email" name="contact_form_email" id="contact_form_email" className="contact_form_email" placeholder="Your Email :"/>&nbsp; &nbsp;
                                    <input type="number" name="contact_form_phone" id="contact_form_phone" className="contact_form_phone" placeholder="Your Phone :"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md p-3">
                                    <textarea className="contact_form_msg" id="contact_form_msg" name="contact_form_msg" rows="4" cols="50" placeholder="message">
                                    </textarea>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md p-3">
                                    <button type="submit" className="msg-btn" id="msg-btn">SUBMIT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Contact
