import React from 'react'
import './ErrorPage.css'
import {Link} from 'react-router-dom'

const ErrorPage = () => {
    return (
        <>
            <div id="err-pg">
                <div className="not-found">
                    404
                </div>
                <div className="err-msg">
                    The Page You are Wondering is not Available
                </div>
                <br/>
                <Link to="/" className="err-btn">Go to Home</Link>
            </div> 
        </>
    )
}

export default ErrorPage
