import React,{useState} from 'react'
import './LoginForm.css'
import {Link,useHistory} from 'react-router-dom';


const SignupForm = () => {

    const history = useHistory();

    const[user,setUser] = useState({
        name:"",email:"",phone:"",work:"",password:"",cpassword:""
    });

    let name,value;
    const handleInputs = (e) => {
        console.log(e);
        name = e.target.name;
        value = e.target.value;

        setUser({...user,[name]:value})
    }

    const PostData = async(e) => {
        e.preventDefault();

        const {name,email,phone,work,password,cpassword} = user;

        const res = await fetch("/signup",{
            method:"POST",
            headers:{
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                name:name, email:email, phone:phone, work:work, password:password, cpassword:cpassword
            })
        });

        const data = await res.json();

        if(data.status === 422 || !data){
            window.alert("Registtraion Failed.")
        }else if(data.status === 500){
            window.alert("Server Error");
        }else{
            window.alert("Registration Done.")
            history.push("/login");
        }
    }

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-8 login-box">

                        <div className="col-lg-12 login-key">
                            <i className="zmdi zmdi-laptop-mac"></i>
                        </div>

                        <div className="col-lg-12 login-title">
                            SIGNUP PANEL
                        </div>

                        <div className="col-lg-12 login-form">
                            <div className="col-lg-12 login-form">
                                <form id="signup-form" method="POST">

                                <div className="form-group">
                                    <label className="form-control-label">NAME</label>
                                    <i className="zmdi zmdi-account-circle"></i>
                                    <input type="text" name="name" value={user.name} onChange={handleInputs} id="name" placeholder="Enter Name" className="form-control"/>
                                </div>

                                <div className="form-group">
                                    <label className="form-control-label">EMAIL</label>
                                    <input type="email" name="email" value={user.email} onChange={handleInputs} id="email" placeholder="Enter Email" className="form-control"/>
                                </div>

                                <div className="form-group">
                                    <label className="form-control-label">PHONE</label>
                                    <input type="number" name="phone" value={user.phone} onChange={handleInputs} id="phone" placeholder="Enter Number" className="form-control"/>
                                </div>

                                <div className="form-group">
                                    <label className="form-control-label">WORK</label>
                                    <input type="text" name="work" value={user.work} onChange={handleInputs} id="work" placeholder="Enter Profession" className="form-control"/>
                                </div>

                                <div className="form-group">
                                    <label className="form-control-label">PASSWORD</label>
                                    <input type="password" name="password" value={user.password} onChange={handleInputs} id="password" placeholder="Your password" className="form-control" i/>
                                </div>


                                <div className="form-group">
                                    <label className="form-control-label">CONFIRM PASSWORD</label>
                                    <input type="password" name="cpassword" value={user.cpassword} onChange={handleInputs} id="cpassword" placeholder="Confirm Password" className="form-control" i/>
                                </div>

                                <div className="col-lg-12 loginbttm">
                                    <div className="col-lg-6 login-btm login-text">
                                    </div>

                                    <div className="col-lg-6 login-btm login-button">
                                        <button type="submit" name="signup" id="signup" onClick={PostData} className="btn btn-outline-primary">SIGNUP</button>
                                    </div>
                                    <Link to="/login">Already have an account ?</Link>

                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default SignupForm
