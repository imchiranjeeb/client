import React,{useState} from 'react'
import './LoginForm.css'
import {Link,useHistory} from 'react-router-dom';

const LoginForm = () => {

    const history = useHistory();
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');

    const changeEmail = (e) =>{
        setEmail(e.target.value);
    }

    const changePassword = (e) =>{
        setPassword(e.target.value);
    }

    const logInUser = async(e) => {
        e.preventDefault();

        const res = await fetch('/login',{
            method: 'POST',
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                email:email,
                password:password
            })
        });

        const data = res.json();

        if (res.status === 400 || !data){
            window.alert("Invalid Credentials.")
        }else if(res.status === 404){
            window.alert("User not Registered.")
        }else if(res.status === 500){
            window.alert("Server Problems.")
        }else{
            window.alert("Logged In....");
            history.push("/");
        }
    }

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-8 login-box">

                        <div className="col-lg-12 login-key">
                            <i class="zmdi zmdi-key"></i>
                        </div>

                        <div className="col-lg-12 login-title">
                            LOGIN PANEL
                        </div>

                        <div className="col-lg-12 login-form">
                            <div className="col-lg-12 login-form">
                                <form method="POST">
                                <div className="form-group">
                                    <label className="form-control-label">EMAIL</label>
                                    <input type="email" className="email" onChange={changeEmail} value={email} name="email" id="email"placeholder="Enter Email" className="form-control"/>
                                </div>

                                <div className="form-group">
                                    <label className="form-control-label">PASSWORD</label>
                                    <input type="password" name="password" onChange={changePassword} value={password} id="password" placeholder="Enter your password"className="form-control" i/>
                                </div>

                                <div className="col-lg-12 loginbttm">
                                    <div className="col-lg-6 login-btm login-text">
                                
                                    </div>

                                    <div className="col-lg-6 login-btm login-button">
                                        <button type="submit" onClick={logInUser} id="login" name="login" className="btn btn-outline-primary">LOGIN</button>
                                    </div>

                                    <Link to="/signup">Do n't have an account ?</Link>

                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default LoginForm
