import React,{useEffect} from 'react'
import './About.css'
import {useHistory} from 'react-router-dom'
import myPic from '../../images/myImg.jpg'

const About = () => {

    const history = useHistory();

    const callAboutPage = async ()=>{
        try {
            const res = await fetch('/about',{
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    "Content-Type": "application/json"
                },
                credentials: "include"
            });
            const data = await res.json();
            console.log(data)

            if(!res.status === 200) {
                window.alert(res.error)
                throw new Error(res.error)
            }
        } catch (e) {
            console.log(e.message);
            history.push("/login");
        }
    }

    useEffect(() => {
        callAboutPage();
    },[]);

    return (
        <>
            <div id="about-page">
                <div className="container">
                    <form method="GET">
                        <div className="row">
                            <div className="col-md-4">
                                <img src ={myPic} alt="chiru"/>
                            </div>
                            <div className="col-md-4">
                                <h5>Looser</h5>
                                <h6>Unemployeed</h6>
                                <p>Ranking : 0/10</p>
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item">
                                        <a className="nav-link active" id="home-tab" role="tab" data-toggle="tab" href="#home">About</a>
                                    </li>

                                    <li className="nav-item">
                                        <a className="nav-link" role="tab" id="profile-tab" data-toggle="tab" href="#profile">Timeline</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-md-2">
                                <input type="text" className="edit-profile-btn" value="Edit Profile"/>
                            </div>                            
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <p>WORK LINK</p>
                                <a href="https://github.com/iamchiranjeeb" target="_chiru">GitHub</a><br/>
                                <a href="https://bitbucket.org/imchiranjeeb/" target="_chiru">Bit-BUcket</a><br/>
                                <a href="https://gitlab.com/iamchiranjeeb" target="_chiru">GitLab</a><br/>
                                <a href="https://www.youtube.com/channel/UClxFGhP7uZjhmXwgf3gRoHQ" target="_chiru">Youtube</a><br/>
                                <a href="https://www.linkedin.com/in/chandan-chiranjeeb-sahoo-862a2b183/" target="_chiru">LinkdIn</a><br/>
                            </div>
                            <div className="col-md-8 pl-5">
                                <div className="" id="myTabContent">
                                    <div className="tab-pane fade show active" aria-labelledby="about-page" id="home" role="tabpanel">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> USER ID </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>1601289503 </p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> NAME: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>Chandan Chiranjeeb</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> EMAIL: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>chandansahoo438@gmail.com</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> PHONE: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>9348768387</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> PROFESSION: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>Unemployeed</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" aria-labelledby="profile-tab" id="profile" role="tabpanel">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label> EXPERIENCE: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>Zero </p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label> HOURLY RATE: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>50</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label> TOTAL PROJECTS: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>2</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> ENGLISH LEVEL: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>Medium</p>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-md-6">
                                                <label htmlFor="User ID"> AVALIABLE: </label>
                                            </div>
                                            <div className="col-md-6">
                                               <p>15 Days</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default About
